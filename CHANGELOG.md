### 18.0.0 Update to Angular 18.
* 8c2ccf3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 1531caa -- Merge branch 'develop' into 'master'
* 8e963a4 -- Add update to VERSION.txt
* 7fcedaf -- Merge branch '13-update-to-angular-18' into 'develop'
* 9545eb4 -- Refactor theme.scss
* 8ab76f3 -- Update to Angular 18
### 16.0.2 Update visualization versions.
* 59c6d5d -- [CI/CD] Update packages.json version based on GitLab tag.
* 2436573 -- Merge branch 'update-visualization-versions' into 'master'
* b8f2fd9 -- Update visualization versions.
### 16.0.1 Update sentinel versions.
* 68d63d8 -- [CI/CD] Update packages.json version based on GitLab tag.
* a300073 -- Merge branch 'update-sentinel-versions' into 'master'
* 797e330 -- Update sentinel versions.
### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* 1f84a78 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9040344 -- Merge branch '11-update-to-angular-16' into 'master'
* 96aff09 -- Update to Angular 16
### 15.0.0 Update to Angular 15
* aa86695 -- [CI/CD] Update packages.json version based on GitLab tag.
* a4c26fe -- Merge branch '10-update-to-angular-15' into 'master'
* 3870ef0 -- Update to Angular 15
### 14.1.2 Enable card hiding if there is insufficient data for clustering
* 47c7fc3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5da8a50 -- Merge branch '9-hide-chart-views-when-empty' into 'master'
* 1911e3d -- Update package-lock
* ff74413 -- Satisfy prettier
* 4ef6dad -- Fix chart display on wrong data, repair tooltip
* 453a042 -- Fix scatterplot chart display without data
* 8653068 -- Add function to hide scatter
* 03a93e9 -- Enable radar chart wrapper hiding
* 2e17ec1 -- Hide empty clusters without TI
### 14.1.1 Update clustering with new API calls
* f3d9d1a -- [CI/CD] Update packages.json version based on GitLab tag.
* 506eeb6 -- Merge branch '8-update-clustering-vis-version-with-new-api' into 'master'
* 15f90af -- Resolve "Update clustering vis version with new API"
### 14.1.0 Add clustering and overview visualization.
* 4824571 -- [CI/CD] Update packages.json version based on GitLab tag.
* 977dc18 -- Tag version
*   96028d2 -- Merge branch '7-integrate-ml-charts-into-the-dashboard-2' into 'master'
|\  
| * e3b9298 -- Resolve "Integrate ML charts into the dashboard"
|/  
* b8620a1 -- Fix styling problem
* 8a81231 -- Merge branch '6-add-clustering-visualization-to-dashboard' into 'master'
* 76cd235 -- Resolve "Add clustering visualization to dashboard"
### 14.0.0 Update to Angular 14
* 8ea8674 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c71f520 -- Merge branch '5-update-to-angular-14' into 'master'
|\  
| * a37253d -- Resolve "Update to Angular 14"
|/  
* 856ed9b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 1134fb3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4a74638 -- Merge branch '4-fix-scatterplot-userrefid-and-percentage-edge-cases-in-barchart-diagram' into 'master'
* 4a2fdb1 -- Resolve "Fix scatterplot userRefId and percentage edge cases in barchart diagram"
### 13.0.1 Fix scatterplot user id, add event name to filtering table. Combined diagram now shows instances by their id instead of event date. Fix percentage in barchart.
* 1134fb3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4a74638 -- Merge branch '4-fix-scatterplot-userrefid-and-percentage-edge-cases-in-barchart-diagram' into 'master'
* 4a2fdb1 -- Resolve "Fix scatterplot userRefId and percentage edge cases in barchart diagram"
### 13.0.1-rc.2 Renamed flag to answer in visualizations. Fixed console undefined errors. Fixed a bug in the computation of wrong answers.
* 0aacc73 -- [CI/CD] Update packages.json version based on GitLab tag.
*   95553a0 -- Merge branch '3-fix-problems-found-during-release-testing' into 'master'
|\  
| * 755ebe8 -- Resolve "Fix problems found during release testing"
|/  
* d1ebc45 -- Merge branch '2-emit-instance-id-on-detail-view' into 'master'
* 74d1300 -- Resolve "Emit instance id on detail view"
### 13.0.1-rc.1 Addressing issues found during the testing of the latest release.
* cac3aa1 -- [CI/CD] Update packages.json version based on GitLab tag.
* 946501a -- Added tag message
