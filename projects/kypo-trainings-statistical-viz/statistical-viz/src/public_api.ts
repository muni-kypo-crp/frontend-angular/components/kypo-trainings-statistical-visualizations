/*
 * Public API Surface of @muni-kypo-crp/statistical-visualizations/statistical-viz
 */

export * from './statistical-viz.component';
export * from './statistical-visualization.module';
