# KYPO Trainings Statistical Visualizations

This dashboard serves for display and exploration of collected data from multiple training instances in a single training definition.

## How to use json-server as mock backend with provided dummy data

1.  Run `npm install`.
2.  Run the server with provided parameters via `npm run api`.
3.  Run the app in local environment and ssl `npm start` and access it on `https://localhost:4200`.
